import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'pt-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Peachtree Bank';

  public $updateHeartbeat: BehaviorSubject<number> = new BehaviorSubject(0);

  onSignal(time:number){
    this.$updateHeartbeat.next(time);
  }
}
