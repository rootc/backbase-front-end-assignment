import { Component, OnInit, Input } from '@angular/core';
import { Transaction } from '../model/TransactionList';

@Component({
  selector: 'pt-trasaction-item-ui',
  templateUrl: './trasaction-item-ui.component.html',
  styleUrls: ['./trasaction-item-ui.component.css']
})
export class TrasactionItemUiComponent  implements OnInit{

  @Input() transaction : Transaction | undefined;

  constructor() { }

  ngOnInit():void {
    
  }

}
