import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrasactionItemUiComponent } from './trasaction-item-ui.component';

describe('TrasactionItemUiComponent', () => {
  let component: TrasactionItemUiComponent;
  let fixture: ComponentFixture<TrasactionItemUiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrasactionItemUiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrasactionItemUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
