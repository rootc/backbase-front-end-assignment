
export interface TransactionList {
  data: Array<Transaction>;
}

export interface Transaction {
  amount: string;
  categoryCode: string;
  merchant: string;
  merchantLogo: string;
  transactionDate: Date;
  transactionType: string;
}
