import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { TransactionWidgetComponent } from './transaction-widget/transaction-widget.component';

import { TrasactionsService } from './service/trasactions.service';
import { TrasactionItemUiComponent } from './trasaction-item-ui/trasaction-item-ui.component'

@NgModule({
  declarations: [TransactionWidgetComponent, TrasactionItemUiComponent],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [TrasactionsService],
  exports: [TransactionWidgetComponent]
})
export class TransactionWidgetAngModule { }
