import { Component, OnInit, ChangeDetectorRef, Input, OnDestroy } from '@angular/core';
import { TrasactionsService } from '../service/trasactions.service';
import { TransactionList, Transaction } from '../model/TransactionList';
import { BehaviorSubject, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'pt-transaction-widget',
  templateUrl: './transaction-widget.component.html',
  styleUrls: ['./transaction-widget.component.css']
})
export class TransactionWidgetComponent implements OnInit, OnDestroy {

  @Input() updateHeartbeat: BehaviorSubject<number> | undefined;
  private unsubscribeUpdateHeartbeat: Subscription | undefined;

  public list: Array<Transaction> = [];
  private rawList: Array<Transaction> = []

  public sortType:string ="DATE";
  public sortOrderDsc:boolean = true;

  constructor(private readonly service: TrasactionsService, private cd: ChangeDetectorRef) { }
  

  ngOnInit(): void {
    this.loadTransactions(true);
    if(this.updateHeartbeat){
      this.unsubscribeUpdateHeartbeat = this.updateHeartbeat.subscribe((change:number) =>{
        //with the number we can limit updates etc.
        if(change !== 0){
          this.loadTransactions(false);
        }
      });

    } else {
      console.error("updateHeartbeat not provided. List will not refresh.")
    }

  }

  sortByDate( click:boolean = true ): void {
    if(click) {
      this.sortOrderDsc = (this.sortType === "DATE") ? !this.sortOrderDsc : this.sortOrderDsc;
    }
    if(this.sortOrderDsc){
      this.list.sort((a, b) => new Date(b.transactionDate).getTime() - new Date(a.transactionDate).getTime());
    }else {
      this.list.sort((b, a) => new Date(b.transactionDate).getTime() - new Date(a.transactionDate).getTime());
    }
    this.sortType="DATE";
    this.cd.markForCheck();
  }

  sortByBeneficiary( click:boolean = true ): void {
    if(click) {
      this.sortOrderDsc = (this.sortType === "BENEFICIARY") ? !this.sortOrderDsc : this.sortOrderDsc;
    }
    if(this.sortOrderDsc){
      this.list.sort((a, b) => (a.merchant < b.merchant) ? 1 : -1 );
    }else {
      this.list.sort((a, b) => (a.merchant > b.merchant) ? 1 : -1 );
    }
    this.sortType = "BENEFICIARY";
    this.cd.markForCheck();
  }

  sortByAmount( click:boolean = true ): void {
    if(click) {
      this.sortOrderDsc = (this.sortType === "AMOUNT") ? !this.sortOrderDsc : this.sortOrderDsc;
    }
    if(this.sortOrderDsc){
      this.list.sort((a, b) => (parseFloat(a.amount) < parseFloat(b.amount)) ? 1 : -1 );
    }else {
      this.list.sort((a, b) => (parseFloat(a.amount) > parseFloat(b.amount)) ? 1 : -1 );    
    }
    this.sortType = "AMOUNT";
    this.cd.markForCheck();
  }

  onSearchChange(value:string): void {
    this.list = this.rawList;
    this.list =this.list.filter( a => { 
      return a.merchant.toUpperCase().includes(value.toUpperCase())
    });
  }

  private loadTransactions(init:boolean) {
    this.service.getTransactions().pipe(take(1)).subscribe( res => {

      if(!init){
        //For requirement new transations be on top. If we want to keep the filter sorting should be run again with 'false' as input.
        this.list.unshift(res.data[0]);
      } else {
        this.list = res.data;
      }
      this.rawList = res.data;
    })
  }

  ngOnDestroy(): void {
    if(this.unsubscribeUpdateHeartbeat){
      this.unsubscribeUpdateHeartbeat.unsubscribe();
    }
  }
}
