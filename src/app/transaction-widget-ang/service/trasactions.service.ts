import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import { TransactionList } from '../model/TransactionList';

@Injectable({
  providedIn: 'root'
})
export class TrasactionsService {


  constructor(private httpClient: HttpClient) {
  }

  getTransactions(): Observable<TransactionList> {
    return this.httpClient
               .get<TransactionList>(`https://example.com/api/transactions`);
               
  }
}
