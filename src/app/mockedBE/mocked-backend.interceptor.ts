import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import * as jsonTransactions from './transactions.json';
import {Image} from './sampleImage';

export interface PostTransfer {
  fromId: string,
  beneficiary: string,
  amount: number
}

export interface TransferResponse {
  completed:boolean,
  message?: string
}

export interface TransactionList {
  data: Array<Transaction>;
}

export interface Transaction {
  amount: string;
  categoryCode: string;
  merchant: string;
  merchantLogo: string;
  transactionDate: number;
  transactionType: string;
}




@Injectable()
export class MockedBackendInterceptor implements HttpInterceptor {

  private account = {
      id: "ax1239fscsf",
      name: "Free Checking(4692)",
      balance: 5824.76
    }
  
  private transfers: Array<Transaction> = []

  constructor(private injector: Injector) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    if (request.url === 'https://example.com/api/transfer') {
      console.log('Request to :' + request.url);

      const response = this.submitPayment(request.body)
      return of(new HttpResponse({ status: 200, body: response }));
    }
    
    if (request.url === 'https://example.com/api/getAccount') {
      console.log('Request to :' + request.url);
      return of(new HttpResponse({ status: 200, body: this.account }));
    }

    if (request.url === 'https://example.com/api/transactions') {
      console.log('Request to :' + request.url);
      return of(new HttpResponse({ status: 200, body: this.getTransactions() }));
    }

    return next.handle(request);
  }

  submitPayment( body : PostTransfer) {
    if(this.account.balance - body.amount <= -500){
      return  { completed: false, message: "Overdraft limit reached"}
    } else {

      this.account.balance = this.account.balance - body.amount;
      const transfer:Transaction = {
        amount: body.amount.toFixed(),
        categoryCode: '#1180aa',
        merchant: body.beneficiary,
        merchantLogo: Image,
        transactionDate: new Date().getTime(),
        transactionType: "Online transfer"
      }
      this.transfers.push(transfer);
      return { completed: true};

    }
  }

  getTransactions(){
    const loadJson:TransactionList =  <TransactionList> (jsonTransactions as any).default;
    let response:TransactionList = {data:[...this.transfers, ...loadJson.data ]}

    return response;
  }
}

