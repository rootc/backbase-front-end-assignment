import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms'

import { TransferWidgetComponent } from './transfer-widget/transfer-widget.component';

import { TrasactionsService } from '../transaction-widget-ang/service/trasactions.service';



@NgModule({
  declarations: [TransferWidgetComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  providers: [TrasactionsService, CurrencyPipe],
  exports: [TransferWidgetComponent]
})
export class TransferWidgetAngModule { }
