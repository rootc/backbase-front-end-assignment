import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';

import {Account} from '../model/account';

export interface PostTransfer {
  fromId: string,
  beneficiary: string,
  amount: number
}

export interface TransferResponse {
  completed:boolean,
  message?: string
}

@Injectable({
  providedIn: 'root'
})
export class TransferService {

  constructor(private httpClient: HttpClient) { }

  getAccount(): Observable<Account> {
    return this.httpClient
               .get<Account>(`https://example.com/api/getAccount`);
               
  }

  postTransfer(fromId:string, toAccount:string, amount:number ): Observable<TransferResponse> {
    const body : PostTransfer = {
      fromId: fromId,
      beneficiary: toAccount,
      amount: amount
    }
    return this.httpClient
               .post<TransferResponse>(`https://example.com/api/transfer`, body);
  }
}
