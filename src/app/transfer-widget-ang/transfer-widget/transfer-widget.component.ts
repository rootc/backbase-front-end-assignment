import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {take} from 'rxjs/operators';
import { CurrencyPipe } from '@angular/common';

import { TransferService, TransferResponse } from '../service/transfer.service';
import { Account } from '../model/account';


@Component({
  selector: 'pt-transfer-widget',
  templateUrl: './transfer-widget.component.html',
  styleUrls: ['./transfer-widget.component.css']
})
export class TransferWidgetComponent implements OnInit {

  @Output() updateSignal = new EventEmitter<number>();

  transferForm = this.fb.group({
    fromAccount: [ {value: '', disabled: true }],
    toAccount: [''],
    amount:['']
  })

  private account : Account | undefined;
  public submitedToAccount : string = '';
  public submitedAmount : number = 0;

  public formSubmited : boolean = false;

  constructor(private fb: FormBuilder, private service:TransferService,private currencyPipe : CurrencyPipe) { }

  ngOnInit(): void {
    this.initForm();
  }

  noSigns(event:any) {
    return event.key !== '-' && event.key !== '+' && event.key !== 'e';
  }
  onSubmit(){
    this.submitedToAccount = this.transferForm.value.toAccount;
    this.submitedAmount = this.transferForm.value.amount;
    this.formSubmited = true;
  }

  onCancel(){
    this.formSubmited = false;
  }

  onConfirm() {
    this.formSubmited = false;
    if ( this.account ) {
      this.service.postTransfer(this.account.id, this.submitedToAccount, this.submitedAmount).pipe(take(1)).subscribe( (res:TransferResponse) => {
        if(res.completed){
          this.updateSignal.next(new Date().getTime());
        } else {
          alert(res.message || "Transfer failed");
        }
        
      });
    } else {
      console.error("Account not loaded.")
    }
    this.initForm();
  }

  fromAccountData(): string{
    if(this.account){
      return this.account.name+" "+ this.currencyPipe.transform(this.account.balance);
    } else {
      return '';
    }
  }

  private initForm() {
    this.transferForm.reset();
    this.service.getAccount().pipe(take(1)).subscribe( (res : Account)  => {
      this.transferForm.get("fromAccount")?.setValue(res.name+" "+ this.currencyPipe.transform(res.balance));
      this.account = res;
    })

  }



}
