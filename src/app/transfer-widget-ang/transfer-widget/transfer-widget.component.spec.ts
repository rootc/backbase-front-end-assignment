import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferWidgetComponent } from './transfer-widget.component';

describe('TransferWidgetComponent', () => {
  let component: TransferWidgetComponent;
  let fixture: ComponentFixture<TransferWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
