import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TransferWidgetAngModule } from './transfer-widget-ang/transfer-widget-ang.module';
import { TransactionWidgetAngModule } from './transaction-widget-ang/transaction-widget-ang.module';



import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MockedBackendInterceptor } from './mockedBE/mocked-backend.interceptor'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    TransferWidgetAngModule,
    TransactionWidgetAngModule
  ],
  providers: [
    {
         provide: HTTP_INTERCEPTORS,
         useClass: MockedBackendInterceptor,
         multi: true
         }
     ],
  bootstrap: [AppComponent]
})
export class AppModule { }
