# Peachtree Bank Example

This project was created for a  Backbase Front End Assignment: Make A Transaction

## Setup and run the project

- Clone (or unzip)
- To install all required packages run `npm i`
- To start the project `npm start`

## Application structure

The root of the project contains all the basic angular files.
This is an Angular application so the file structure is the same some changes.
Notable change there are 2 modules that independent and act as widgets.

`src/app/mockedBE` contains the interceptor and logic that simulates API responses.
`src/app/transaction-widget-ang` is a one of the 2 widgets as the name implies this is the transactions widget.
`src/app/transfer-widget-ang` is the second widget, transfer widget.

Widgets are created in a way that can be exported and be used with little to no modifications.
Ideally all the css would be in a separate theme folder so they can be modified customized without rebuilding the app. Due to time constrains theme was not created.

The main app is just a container housing the widgets and an update mechanism.
All data is processed via the BE server, mocked in this example.

i18n support is added but no translations are provided.


## update: link to hosted page
https://admiring-northcutt-ee4ba5.netlify.app/